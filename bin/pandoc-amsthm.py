#!/usr/bin/env python

"""
A pandoc filter to define amsthm environement through YAML, the use of Div,
with templates and this filter.
See detail in https://github.com/ickc/pandoc-amsthm
Credit: https://github.com/chdemko/pandoc-latex-environment
"""

from pandocfilters import toJSONFilters, RawBlock, stringify

import re

def latex_command(name, content):
    if content == "":
        return ""
    else:
        return '\\' + name + '{' + content + '}'

def begin(x):
    return latex_command('begin', x)

def end(x):
    return latex_command('end', x)

def latex(x):
    return RawBlock('tex', x)

def brackets(name):
    if name == "":
        return ""
    else:
        return '[' + name + ']'

def environment(key, value, format, meta):
    # Is it a div and the right format?
    if key == 'Div' and format == 'latex':

        # Get the attributes
        [[id, classes, properties], content] = value
        currentClasses = set(classes)

        for environment, definedClasses in getDefined(meta).items():
            for definedClass in definedClasses:
                # Is the classes correct?
                if definedClass in currentClasses:
                    lbl = latex_command('label', id) if id != "" else ""
                    name = ""
                    for k,v in properties:
                        if k == "name":
                            name = brackets(v)

                    if id != "":
                        lbl = latex_command('label', id)

                    value[1] = [latex(begin(definedClass) + name + lbl)]  + content + [latex(end(definedClass))]
                    break

def getDefined(meta):
    # Return the amsthm environment defined in the meta
    if not hasattr(getDefined, 'value'):
        getDefined.value = {}
        if 'amsthm' in meta and meta['amsthm']['t'] == 'MetaMap':
            for environment, classes in meta['amsthm']['c'].items():
                if classes['t'] == 'MetaList':
                    getDefined.value[environment] = []
                    for klass in classes['c']:
                        string = stringify(klass)
                        if re.match('^[a-zA-Z][\w.:-]*$', string):
                            getDefined.value[environment].append(string)
                    getDefined.value[environment] = set(getDefined.value[environment])
    return getDefined.value

def main():
    toJSONFilters([environment])

if __name__ == '__main__':
    main()

